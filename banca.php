<html>
<head>
    <title>Banca</title>
    <meta charset="UTF-8">
    <meta name="description" content="Sites">
    <meta name="keywords" content="HTML,CSS,PHP,JavaScript,MySql">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Pavle Poljcic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
</html>
<?php

require_once  'model.php';

$model = new model();
$header_menu = $model -> GetMenu(3, 3);
$site = $model->Sites();
?>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-dark d-flex">
            <a href="<?=$site[2]['SITE_LINK'];?>" class="text-white mr-md-3"><h4><?=$site[2]['NAME'];?></h4></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav py-3">
                    <?php
                    if(isset($_GET['id']))
                    {
                    $menu3 = $model->GetMenu(3, $_GET['id']);
                    foreach ($menu3 as $row):
                        $item = $model -> GetChild($row['ELEMENT_ID']);
                        if(!empty($item)): ?>
                            <li class="nav-item dropdown mega-dropdown">
                                <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false"><?=$row['LABEL'];?></a>
                                <div class="dropdown-menu mega-menu v-2 z-depth-1 bg-dark" aria-labelledby="navbarDropdownMenuLink2">
                                    <ul class="list-unstyled">
                                        <?php foreach ($item as $value): ?>
                                            <li class="nav-item"><a class="nav-link text-white" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </li>
                        <?php else: ?>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a>
                            </li>
                    <?php endif; endforeach; }
                    else {
                        $menu3 = $model->GetMenu(3, 3);
                        foreach ($menu3 as $row):
                            $item = $model -> GetChild($row['ELEMENT_ID']);
                            if(!empty($item)): ?>
                                <li class="nav-item dropdown mega-dropdown">
                                    <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false"><?=$row['LABEL'];?></a>
                                    <div class="dropdown-menu mega-menu v-2 z-depth-1 bg-dark" aria-labelledby="navbarDropdownMenuLink2">
                                        <ul class="list-unstyled">
                                            <?php foreach ($item as $value): ?>
                                                <li class="nav-item"><a class="nav-link text-white" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </li>
                            <?php else: ?>
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a>
                                </li>
                            <?php endif; endforeach;
                    }?>
                </ul>
            </div>
        </nav>
        <div class="row d-flex align-items-center">
            <div class="col-12 col-md-6">
                <div class="text-center">
                    <p class="align-content-center"><h1>Helping your business grow<br> to the next level.</h1></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras pharetra at velit dolor fames.</p>
                    <button class="btn-dark px-5 py-2 rounded-pill shadow-none border-0" style="outline: none;">Open acount</button>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <img src="images/banca.png" class="img-fluid">
            </div>
        </div>
        <div class="card-group">
            <div class="card rounded">
                <img src="images/banca1new.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card rounded">
                <img src="images/banca2new.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card rounded">
                <img src="images/banca3new.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-12 col-md-4 text-center text-md-left">
                <p class=""><h3 class="">PARTNERS</h3></p>
            </div>
            <div class="col-2 col-md-1 pt-md-4 pt-2 offset-md-1 border">
                <img src="images/partner1.png" class="img-fluid">
            </div>
            <div class="col-2 col-md-1 pt-md-4 pt-2 offset-md-1 border">
                <img src="images/partner3.png" class="img-fluid">
            </div>
            <div class="col-2 col-md-1 pt-md-4 pt-2 offset-md-1 border">
                <img src="images/partner3new.png" class="img-fluid">
            </div>
        </div>
        <!-- Footer -->
        <footer class="page-footer font-small indigo bg-dark mt-3 rounded">

            <!-- Footer Links -->
            <div class="container">

                <!-- Grid row-->
                <div class="row text-center d-flex justify-content-center pt-5 mb-3">
                    <?php foreach ($header_menu as $row): $item = $model -> GetChild($row['ELEMENT_ID']);?>

                    <!-- Grid column -->
                    <div class="col-md-2 mb-3">
                        <h6 class="text-uppercase font-weight-bold">
                            <a href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a>
                        </h6>
                    </div>
                    <!-- Grid column -->
                    <?php endforeach; ?>
                </div>
                <!-- Grid row-->
                <hr class="rgba-white-light" style="margin: 0 15%;">

                <!-- Grid row-->
                <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

                    <!-- Grid column -->
                    <div class="col-md-8 col-12 mt-5 text-white">
                        <p style="line-height: 1.7rem">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium, totam rem
                            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                            explicabo.
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p>
                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row-->
                <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

                <!-- Grid row-->
                <div class="row pb-3">

                    <!-- Grid column -->
                    <div class="col-md-12">

                        <div class="mb-5 text-center">

                            <!-- Facebook -->
                            <a class="fb-ic">
                                <i class="fab fa-facebook-f fa-lg white-text mr-4"> </i>
                            </a>
                            <!-- Twitter -->
                            <a class="tw-ic">
                                <i class="fab fa-twitter fa-lg white-text mr-4"> </i>
                            </a>
                            <!-- Google +-->
                            <a class="gplus-ic">
                                <i class="fab fa-google-plus-g fa-lg white-text mr-4"> </i>
                            </a>
                            <!--Linkedin -->
                            <a class="li-ic">
                                <i class="fab fa-linkedin-in fa-lg white-text mr-4"> </i>
                            </a>
                            <!--Instagram-->
                            <a class="ins-ic">
                                <i class="fab fa-instagram fa-lg white-text mr-4"> </i>
                            </a>
                            <!--Pinterest-->
                            <a class="pin-ic">
                                <i class="fab fa-pinterest fa-lg white-text"> </i>
                            </a>

                        </div>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row-->

            </div>
            <!-- Footer Links -->
    </div>
</body>
