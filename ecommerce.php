<html>
<head>
    <title>E-commerce</title>
    <meta charset="UTF-8">
    <meta name="description" content="Sites">
    <meta name="keywords" content="HTML,CSS,PHP,JavaScript,MySql">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Pavle Poljcic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
</html>
<?php
require_once  'model.php';

$model = new model();
$site = $model->Sites();
//$menu2 = $model -> GetMenu(2,2);
//$item = $model -> GetChild($menu2[2]['ELEMENT_ID']);
$navbar = $model -> Menu(2);

?>
<body>
    <div class="container">
        <?php //echo $navbar['STYLE']; ?>
        <!--Navbar-->
         <nav class="navbar navbar-light bg-light lighten-4">

          <!-- Navbar brand -->
           <a class="navbar-brand" href="<?=$site[1]['SITE_LINK'];?>"><?=$site[1]['NAME'];?></a>

           <!-- Collapse button -->
          <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
                 aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fas fa-bars fa-1x"></i></span></button>

           <!-- Collapsible content -->
             <div class="collapse navbar-collapse" id="navbarSupportedContent1">

             <!-- Links -->
               <ul class="navbar-nav mr-auto text-dark">
                    <?php
                    if(isset($_GET['id']))
                    {
                    $menu2 = $model->GetMenu(2, $_GET['id']);
                    foreach($menu2 as $row) {
                      $item = $model -> GetChild($row['ELEMENT_ID']);
                   if(!empty($item)) {  ?>
                       <div class="dropdown py-2">
                       <a href="#" class="dropdown-toggle text-secondary py-2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label"><?=$row['LABEL'];?></span> <span class="caret"></span></a>
                            <ul class="dropdown-menu border-0 bg-light">
                       <?php foreach($item as $value) { ?>
                                <li class="nav-item ml-2">
                                    <a class="nav-link text-secondary" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a>
                              </li>
                       <?php } ?>
                            </ul>
                        </div>
                   <?php  } else { ?>
                        <li class="nav-item"><a class="nav-link text-secondary py-2" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a></li>
                   <?php } } }
                    else {
                        $menu2 = $model->GetMenu(2, 2);
                        foreach($menu2 as $row) {
                            $item = $model -> GetChild($row['ELEMENT_ID']);
                            if(!empty($item)) {  ?>
                                <div class="dropdown py-2">
                                    <a href="#" class="dropdown-toggle text-secondary py-2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label"><?=$row['LABEL'];?></span> <span class="caret"></span></a>
                                    <ul class="dropdown-menu border-0 bg-light">
                                        <?php foreach($item as $value) { ?>
                                            <li class="nav-item ml-2">
                                                <a class="nav-link text-secondary" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php  } else { ?>
                                <li class="nav-item"><a class="nav-link text-secondary py-2" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a></li>
                            <?php } }
                    }?>
                </ul>
                <!-- Links -->

            </div>
           <!-- Collapsible content -->

       </nav>
        <?php
        //echo ($navbar['STYLE']);
        ?>
        <div class="row mt-2">
            <div class="col-12 col-md-6">
                <img src="images/e%20comrece.jpg" class="img-fluid">
            </div>
            <div class="col-12 col-md-6">
                <h1 class="text-center">About us</h1>
                <p class="text-center e-commerce-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                    a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">

                <!-- Shopping Cart table -->
                <div class="table-responsive">

                    <table class="table">

                        <!-- Table head -->
                        <thead class="mdb-color lighten-5">
                        <tr>
                            <th></th>
                            <th class="font-weight-bold">
                                <strong>Product</strong>
                            </th>
                            <th class="font-weight-bold">
                                <strong>Color</strong>
                            </th>
                            <th></th>
                            <th class="font-weight-bold">
                                <strong>Price</strong>
                            </th>
                            <th class="font-weight-bold">
                                <strong>QTY</strong>
                            </th>
                            <th class="font-weight-bold">
                                <strong>Amount</strong>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <!-- /.Table head -->

                        <!-- Table body -->
                        <tbody>

                        <!-- First row -->
                        <tr>
                            <th scope="row">
                                <img src="images/13.jpg" alt="" class="img-fluid z-depth-0">
                            </th>
                            <td>
                                <h5 class="mt-3">
                                    <strong>iPhone</strong>
                                </h5>
                                <p class="text-muted">Apple</p>
                            </td>
                            <td>White</td>
                            <td></td>
                            <td>$800</td>
                            <td>
                                <input type="number" value="2" aria-label="Search" class="form-control" style="width: 100px">
                            </td>
                            <td class="font-weight-bold">
                                <strong>$800</strong>
                            </td>
                            <td>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top"
                                        title="Remove item">X
                                </button>
                            </td>
                        </tr>
                        <!-- /.First row -->

                        <!-- Second row -->
                        <tr ">
                            <th scope="row">
                                <img src="images/6.jpg" alt="" class="img-fluid z-depth-0">
                            </th>
                            <td>
                                <h5 class="mt-3">
                                    <strong>Headphones</strong>
                                </h5>
                                <p class="text-muted">Sony</p>
                            </td>
                            <td>Red</td>
                            <td></td>
                            <td>$200</td>
                            <td>
                                <input type="number" value="2" aria-label="Search" class="form-control" style="width: 100px">
                            </td>
                            <td class="font-weight-bold">
                                <strong>$600</strong>
                            </td>
                            <td>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top"
                                        title="Remove item">X
                                </button>
                            </td>
                        </tr>
                        <!-- /.Second row -->

                        <!-- Third row -->
                        <tr>
                            <th scope="row">
                                <img src="images/1.jpg" alt="" class="img-fluid z-depth-0">
                            </th>
                            <td>
                                <h5 class="mt-3">
                                    <strong>iPad Pro</strong>
                                </h5>
                                <p class="text-muted">Apple</p>
                            </td>
                            <td>Gold</td>
                            <td></td>
                            <td>$600</td>
                            <td>
                                <input type="number" value="2" aria-label="Search" class="form-control" style="width: 100px">
                            </td>
                            <td class="font-weight-bold">
                                <strong>$1200</strong>
                            </td>
                            <td>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top"
                                        title="Remove item">X
                                </button>
                            </td>
                        </tr>
                        <!-- /.Third row -->

                        <!-- Fourth row -->
                        <tr>
                            <td colspan="3"></td>
                            <td>
                                <h4 class="mt-2">
                                    <strong>Total</strong>
                                </h4>
                            </td>
                            <td class="text-right">
                                <h4 class="mt-2">
                                    <strong>$2600</strong>
                                </h4>
                            </td>
                            <td colspan="3" class="text-right">
                                <button type="button" class="btn btn-primary btn-rounded">Complete purchase
                                    <i class="fas fa-angle-right right"></i>
                                </button>
                            </td>
                        </tr>
                        <!-- Fourth row -->

                        </tbody>
                        <!-- /.Table body -->

                    </table>

                </div>
                <!-- /.Shopping Cart table -->

            </div>

        </div>
        <!-- Footer -->
        <footer class="page-footer font-small blue-grey lighten-5 mt-2">

            <div class="bg-light">
                <div class="container">

                    <!-- Grid row-->
                    <div class="row py-4 d-flex align-items-center">

                        <!-- Grid column -->
                        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                            <h6 class="mb-0">Get connected with us on social networks!</h6>
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-md-6 col-lg-7 text-center text-md-right">

                            <!-- Facebook -->
                            <a class="fb-ic">
                                <i class="fab fa-facebook-f white-text mr-4"> </i>
                            </a>
                            <!-- Twitter -->
                            <a class="tw-ic">
                                <i class="fab fa-twitter white-text mr-4"> </i>
                            </a>
                            <!-- Google +-->
                            <a class="gplus-ic">
                                <i class="fab fa-google-plus-g white-text mr-4"> </i>
                            </a>
                            <!--Linkedin -->
                            <a class="li-ic">
                                <i class="fab fa-linkedin-in white-text mr-4"> </i>
                            </a>
                            <!--Instagram-->
                            <a class="ins-ic">
                                <i class="fab fa-instagram white-text"> </i>
                            </a>

                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row-->

                </div>
            </div>

            <!-- Footer Links -->
            <div class="container text-center text-md-left bg-light">

                <!-- Grid row -->
                <div class="row mt-3 dark-grey-text">

                    <!-- Grid column -->
                    <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

                        <!-- Content -->
                        <h6 class="text-uppercase font-weight-bold">Company name</h6>
                        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                        <p class="e-commerce-text">Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
                            consectetur
                            adipisicing elit.</p>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <?php $footer_menu=$model->GetMenu(2,28);
                    foreach ($footer_menu as $row): ?>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                            <!-- Links -->
                            <h6 class="text-uppercase font-weight-bold"><?=$row['LABEL']?></h6>
                            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                            <?php $item = $model -> GetChild($row['ELEMENT_ID']);
                            foreach ($item as $value): ?>
                            <p class="mb-1">
                                <a class="text-secondary" href="<?=$value['LINK']?>"><?=$value['LABEL']?></a>
                            </p>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                    <!-- Grid column -->

                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

            </div>
            <!-- Footer Links -->

        </footer>
        <!-- Footer -->
    </div>
</body>
