
.mtli_attachment {
	display:inline-block;
	height: auto;
	min-height:64px;
	background-position: top left;
	background-attachment: scroll;
	background-repeat: no-repeat;
	padding-left: 76.8px !important;
}
.mtli_3g2 {
	background-image: url(../images/3g2-icon-64x64.png); }
.mtli_3gp {
	background-image: url(../images/3gp-icon-64x64.png); }
.mtli_ai {
	background-image: url(../images/ai-icon-64x64.png); }
.mtli_air {
	background-image: url(../images/air-icon-64x64.png); }
.mtli_asf {
	background-image: url(../images/asf-icon-64x64.png); }
.mtli_avi {
	background-image: url(../images/avi-icon-64x64.png); }
.mtli_bib {
	background-image: url(../images/bib-icon-64x64.png); }
.mtli_cls {
	background-image: url(../images/cls-icon-64x64.png); }
.mtli_csv {
	background-image: url(../images/csv-icon-64x64.png); }
.mtli_deb {
	background-image: url(../images/deb-icon-64x64.png); }
.mtli_djvu {
	background-image: url(../images/djvu-icon-64x64.png); }
.mtli_dmg {
	background-image: url(../images/dmg-icon-64x64.png); }
.mtli_doc {
	background-image: url(../images/doc-icon-64x64.png); }
.mtli_docx {
	background-image: url(../images/docx-icon-64x64.png); }
.mtli_dwf {
	background-image: url(../images/dwf-icon-64x64.png); }
.mtli_dwg {
	background-image: url(../images/dwg-icon-64x64.png); }
.mtli_eps {
	background-image: url(../images/eps-icon-64x64.png); }
.mtli_epub {
	background-image: url(../images/epub-icon-64x64.png); }
.mtli_exe {
	background-image: url(../images/exe-icon-64x64.png); }
.mtli_f {
	background-image: url(../images/f-icon-64x64.png); }
.mtli_f77 {
	background-image: url(../images/f77-icon-64x64.png); }
.mtli_f90 {
	background-image: url(../images/f90-icon-64x64.png); }
.mtli_flac {
	background-image: url(../images/flac-icon-64x64.png); }
.mtli_flv {
	background-image: url(../images/flv-icon-64x64.png); }
.mtli_gif {
	background-image: url(../images/gif-icon-64x64.png); }
.mtli_gz {
	background-image: url(../images/gz-icon-64x64.png); }
.mtli_ico {
	background-image: url(../images/ico-icon-64x64.png); }
.mtli_indd {
	background-image: url(../images/indd-icon-64x64.png); }
.mtli_iso {
	background-image: url(../images/iso-icon-64x64.png); }
.mtli_jpg {
	background-image: url(../images/jpg-icon-64x64.png); }
.mtli_jpeg {
	background-image: url(../images/jpeg-icon-64x64.png); }
.mtli_key {
	background-image: url(../images/key-icon-64x64.png); }
.mtli_log {
	background-image: url(../images/log-icon-64x64.png); }
.mtli_m4a {
	background-image: url(../images/m4a-icon-64x64.png); }
.mtli_m4v {
	background-image: url(../images/m4v-icon-64x64.png); }
.mtli_midi {
	background-image: url(../images/midi-icon-64x64.png); }
.mtli_mkv {
	background-image: url(../images/mkv-icon-64x64.png); }
.mtli_mov {
	background-image: url(../images/mov-icon-64x64.png); }
.mtli_mp3 {
	background-image: url(../images/mp3-icon-64x64.png); }
.mtli_mp4 {
	background-image: url(../images/mp4-icon-64x64.png); }
.mtli_mpeg {
	background-image: url(../images/mpeg-icon-64x64.png); }
.mtli_mpg {
	background-image: url(../images/mpg-icon-64x64.png); }
.mtli_msi {
	background-image: url(../images/msi-icon-64x64.png); }
.mtli_odp {
	background-image: url(../images/odp-icon-64x64.png); }
.mtli_ods {
	background-image: url(../images/ods-icon-64x64.png); }
.mtli_odt {
	background-image: url(../images/odt-icon-64x64.png); }
.mtli_oga {
	background-image: url(../images/oga-icon-64x64.png); }
.mtli_ogg {
	background-image: url(../images/ogg-icon-64x64.png); }
.mtli_ogv {
	background-image: url(../images/ogv-icon-64x64.png); }
.mtli_pdf {
	background-image: url(../images/pdf-icon-64x64.png); }
.mtli_png {
	background-image: url(../images/png-icon-64x64.png); }
.mtli_pps {
	background-image: url(../images/pps-icon-64x64.png); }
.mtli_ppsx {
	background-image: url(../images/ppsx-icon-64x64.png); }
.mtli_ppt {
	background-image: url(../images/ppt-icon-64x64.png); }
.mtli_pptx {
	background-image: url(../images/pptx-icon-64x64.png); }
.mtli_psd {
	background-image: url(../images/psd-icon-64x64.png); }
.mtli_pub {
	background-image: url(../images/pub-icon-64x64.png); }
.mtli_py {
	background-image: url(../images/py-icon-64x64.png); }
.mtli_qt {
	background-image: url(../images/qt-icon-64x64.png); }
.mtli_ra {
	background-image: url(../images/ra-icon-64x64.png); }
.mtli_ram {
	background-image: url(../images/ram-icon-64x64.png); }
.mtli_rar {
	background-image: url(../images/rar-icon-64x64.png); }
.mtli_rm {
	background-image: url(../images/rm-icon-64x64.png); }
.mtli_rpm {
	background-image: url(../images/rpm-icon-64x64.png); }
.mtli_rtf {
	background-image: url(../images/rtf-icon-64x64.png); }
.mtli_rv {
	background-image: url(../images/rv-icon-64x64.png); }
.mtli_skp {
	background-image: url(../images/skp-icon-64x64.png); }
.mtli_spx {
	background-image: url(../images/spx-icon-64x64.png); }
.mtli_sql {
	background-image: url(../images/sql-icon-64x64.png); }
.mtli_sty {
	background-image: url(../images/sty-icon-64x64.png); }
.mtli_tar {
	background-image: url(../images/tar-icon-64x64.png); }
.mtli_tex {
	background-image: url(../images/tex-icon-64x64.png); }
.mtli_tgz {
	background-image: url(../images/tgz-icon-64x64.png); }
.mtli_tiff {
	background-image: url(../images/tiff-icon-64x64.png); }
.mtli_ttf {
	background-image: url(../images/ttf-icon-64x64.png); }
.mtli_txt {
	background-image: url(../images/txt-icon-64x64.png); }
.mtli_vob {
	background-image: url(../images/vob-icon-64x64.png); }
.mtli_wav {
	background-image: url(../images/wav-icon-64x64.png); }
.mtli_wmv {
	background-image: url(../images/wmv-icon-64x64.png); }
.mtli_xls {
	background-image: url(../images/xls-icon-64x64.png); }
.mtli_xlsx {
	background-image: url(../images/xlsx-icon-64x64.png); }
.mtli_xml {
	background-image: url(../images/xml-icon-64x64.png); }
.mtli_xpi {
	background-image: url(../images/xpi-icon-64x64.png); }
.mtli_zip {
	background-image: url(../images/zip-icon-64x64.png); }