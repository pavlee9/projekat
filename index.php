<html>
<head>
    <title>Sites</title>
    <meta charset="UTF-8">
    <meta name="description" content="Sites">
    <meta name="keywords" content="HTML,CSS,PHP,JavaScript,MySql">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Pavle Poljcic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

</head>
</html>
<?php require_once 'model.php';
$model = new model();
$table = $model->RootitemsbySites();
$sites = $model -> Sites();
$menu = $model -> Menu();
$site_menu = $model -> Sitesandmenus();

?>
<body class="bg-white">
    <p class="bg-dark"><h1 class="text-center">Recording menus and sites</h1></p>
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active text-dark" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sites and menus</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Preview menu on site</a>
            </li>
        </ul>
        <div class="tab-content mt-3" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row justify-content-md-between">
                    <div class="">
                        <button class="btn btn-dark mt-2 mt-md-0 ml-3 text-nowrap px-4" data-toggle="modal" data-target="#exampleModal">Add menu</button>
                        <button class="btn btn-dark mt-2 mt-md-0 ml-3 text-nowrap" data-toggle="modal" data-target="#exampleModal1">Add submenu</button>
                    </div>
                    <div class="text-md-right">
                        <label class="mr-md-2 mt-1 ml-3 ml-md-0">Sort by:</label>
                        <select class="mt-2 mt-md-0 mr-md-3" id="sort">
                            <option>root items by sites</option>
                            <option>alphabetically by root items</option>
                            <option>number of children</option>
                        </select>
                    </div>
                </div>
                <div id="s" class="text-center bg-light"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-dark mt-3" id="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Site name</th>
                            <th scope="col">Menu name</th>
                            <th scope="col">Root item name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $br=0; foreach ($table as $row): ?>
                            <tr>
                                <th scope="row"><?=$br+=1;?></th>
                                <td><?=$row['NAME'];?></td>
                                <td><?=$row['MENU_NAME'];?></td>
                                <td><?=$row['LABEL'];?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <label>Sites</label>
                                <select class="form-control" id="site_select">
                                    <?php foreach ($sites as $row): ?>
                                        <option><?=$row['NAME']?></option>
                                    <?php endforeach; ?>
                                </select><br>
                                <label>Menu*</label>
                                <input class="form-control" type="text" name="menu_name" id="menu_name" placeholder="Menu name" ><br>
                                <label>Root items*</label>
                                <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                                </select>
                                <div class="alert alert-warning mt-2" id="validate" role="alert" style="display: none"></div>
                                <button type="button" class="btn btn-dark mt-2 text-white d-none"  id="add_child" >Add children</button>
                                <div id="tag-list" class="d-none">
                                    <br>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="save">Add</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add children</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="close1" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add submenu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <label>Choose menu*</label>
                            <select class="form-control" id="select_menu">
                                <option></option>
                                <?php foreach ($menu as $row): ?>
                                <option><?=$row['MENU_NAME']?></option>
                                <?php endforeach; ?>
                            </select><br>
                            <div id="select_div" style="display: none">
                                <label>Choose root item</label>
                                <select class="form-control" id="roots">

                                </select><br>
                            </div>
                            <label>Add children*</label>
                            <select class="js-example-basic-multiple" name="childs[]" multiple="multiple">
                            </select>
                            <div class="alert alert-warning mt-2" id="validate1" role="alert" style="display: none"></div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="close1" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="submenu" >Add</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="table-responsive">
                    <table class="table table-striped table-dark mt-3" id="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Site name</th>
                            <th scope="col">Menu name</th>
                            <th scope="col">Click to show menu on site</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $br=0; foreach ($site_menu as $row): ?>
                            <tr>
                                <th scope="row"><?=$br+=1;?></th>
                                <td><?=$row['NAME'];?></td>
                                <td><?=$row['MENU_NAME'];?></td>
                                <td><a class="text-white" href="<?=$row['SITE_LINK'];?>?id=<?=$row['MENU_ID'];?>" target="_blank">Preview<i class="ml-2 fa fa-arrows-alt" aria-hidden="true"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
