<?php


require_once 'connection.php';

class model
{
    public function Sites()
    {
        try
        {
            $sql = 'SELECT * FROM site';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function Menu()
    {
        try
        {
            $sql = 'SELECT * FROM menu';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function Sitesandmenus()
    {
        try
        {
            $sql = 'SELECT site.NAME, site.SITE_LINK, menu.MENU_ID, menu.MENU_NAME FROM menu 
                    INNER JOIN site_menu ON menu.MENU_ID=site_menu.MENU_ID
	                INNER JOIN site ON site_menu.SITE_ID=site.SITE_ID';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function RootitemsbySites()
    {
        try
        {
            $sql = 'SELECT element.LABEL, site.NAME, site.SITE_ID, site.SITE_LINK, menu.MENU_ID, menu.MENU_NAME FROM element 
                    INNER JOIN menu ON element.MENU_ID=menu.MENU_ID
	                INNER JOIN site_menu ON menu.MENU_ID=site_menu.MENU_ID 
	                INNER JOIN site ON site_menu.SITE_ID=site.SITE_ID 
                    WHERE element.PARENT_ID IS NULL';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function RootitemsbySitesAlphabetically()
    {
        try
        {
            $sql = 'SELECT element.LABEL as root_item, site.NAME as site_name, menu.MENU_NAME as menu_name FROM element 
            INNER JOIN menu ON element.MENU_ID=menu.MENU_ID
	        INNER JOIN site_menu ON element.MENU_ID=site_menu.MENU_ID 
	        INNER JOIN site on site_menu.SITE_ID=site.SITE_ID 
            WHERE element.PARENT_ID is null ORDER BY element.LABEL';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            $output = '<thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Site name</th>
                        <th scope="col">Menu name</th>
                        <th scope="col">Root item name</th>
                    </tr>
                    </thead>
            <tbody>';
            $br=1; foreach ($result as $row):
            $output.='<tr>
                                <th scope="row">'.$br++.'</th>
                                <td>'.$row['site_name'].'</td>
                                <td>'.$row['menu_name'].'</td>
                                <td>'.$row['root_item'].'</td>
                            </tr>';
        endforeach;
            $output.='</tbody>';
            echo $output;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function NumberofChilds()
    {
        try
        {
            $sql = 'SELECT parent.LABEL as item, child.num_child FROM element as parent 
            LEFT JOIN (SELECT PARENT_ID, COUNT(*) AS num_child FROM element 
            WHERE PARENT_ID is not null GROUP BY PARENT_ID) as child ON parent.ELEMENT_ID = child.PARENT_ID 
            WHERE parent.PARENT_ID is null ORDER BY child.num_child DESC';
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            $output = '<thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Item</th>
                        <th scope="col">Number of children</th>
                    </tr>
                    </thead>
            <tbody>';
            $br=1; foreach ($result as $row):
            $output.='<tr>
                                <th scope="row">'.$br++.'</th>
                                <td>'.$row['item'].'</td>
                                <td>'.$row['num_child'].'</td>
                            </tr>';
        endforeach;
            $output.='</tbody>';
            echo $output;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetMenu($id_site, $id_menu)
    {
        try
        {
            $sql = 'SELECT element.* FROM menu INNER  JOIN site_menu ON menu.MENU_ID=site_menu.MENU_ID 
                    INNER JOIN site ON site_menu.SITE_ID=site.SITE_ID
                    INNER JOIN element ON menu.MENU_ID=element.MENU_ID WHERE element.PARENT_ID is null AND site.SITE_ID='.$id_site.' AND menu.MENU_ID='.$id_menu;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetChild($id_item)
    {
        try
        {
            $sql = 'SELECT * FROM element WHERE element.PARENT_ID='.$id_item;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetMenuId($menu)
    {
        try
        {
            $sql = "SELECT MENU_ID FROM menu WHERE menu.MENU_NAME='".$menu."'";
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetch();
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetRoots($menu_id)
    {
        try
        {
            $sql = 'SELECT element.LABEL, element.ELEMENT_ID FROM element WHERE element.PARENT_ID is null AND element.MENU_ID='.$menu_id;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll();
            $output = '';
            foreach ($result as $row)
            {
                $output .= '<option>'.$row['LABEL'].'</option>';
            }
            echo $output;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetSiteId($site)
    {
        try
        {
            $sql = "SELECT SITE_ID FROM site WHERE site.NAME='".$site."'";
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetch();
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetRootId($name)
    {
        try
        {
            $sql = "SELECT ELEMENT_ID FROM element WHERE element.LABEL='".$name."'";
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetch();
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetMenuIdWithSiteId($id)
    {
        try
        {
            $sql = "SELECT MENU_ID FROM site_menu WHERE site_menu.SITE_ID=".$id;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetch();
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function updateMenuStyle($id, $style)
    {
        try
        {
            $sql = 'UPDATE menu SET menu.STYLE=:style WHERE menu.MENU_ID=:id';
            $query = database::Connect()->prepare($sql);
            $query->execute(array(':style' => $style, ':id' => $id['MENU_ID']));
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function insertItem($id, $name, $link)
    {
        print_r($name,$link);
        try
        {
            $sql = "INSERT INTO element ( MENU_ID, LABEL, LINK) VALUES ( '$id' , '$name', '$link')";
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $last_id = database::Connect()->lastInsertId();
            return $last_id;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function insertChildItem($id, $parent, $name, $link)
    {
        print_r($name,$link);
        try
        {
            $sql = "INSERT INTO element ( MENU_ID, PARENT_ID, LABEL, LINK) VALUES ( '$id' ,'$parent', '$name', '$link')";
            $query = database::Connect()->prepare($sql);
            $query->execute();

        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function insertMenu($menu_name)
    {
        try
        {
            $sql = "INSERT INTO menu (MENU_NAME) VALUES ('$menu_name')";
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $last_id = database::Connect()->lastInsertId();
            return $last_id;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function insertMenuwithSite($id_site, $id_menu)
    {
        try
        {
            $sql = "INSERT INTO site_menu (MENU_ID, SITE_ID) VALUES ('$id_menu', '$id_site')";
            $query = database::Connect()->prepare($sql);
            $query->execute();
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    public function GetSiteMenu($id_site)
    {
        try
        {
            $sql = 'SELECT * FROM menu INNER  JOIN site_menu ON menu.MENU_ID=site_menu.MENU_ID 
                    INNER JOIN site ON site_menu.SITE_ID=site.SITE_ID
                    WHERE site.SITE_ID='.$id_site;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }

    /*public function Menu($id)
    {
        try
        {
            $sql = 'SELECT * FROM menu WHERE MENU_ID='.$id;
            $query = database::Connect()->prepare($sql);
            $query->execute();
            $result = $query->fetch(PDO::FETCH_ASSOC);
            return $result;
        }
        catch(PDOException $e)
        {
            echo  $e->getMessage();
        }
    }*/
}
