<html>
    <head>
        <title>News site</title>
        <meta charset="UTF-8">
        <meta name="description" content="Sites">
        <meta name="keywords" content="HTML,CSS,PHP,JavaScript,MySql">
        <meta name="author" content="Pavle Poljcic">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="script.js"></script>
        <link rel="stylesheet" href="style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
</html>
<?php
require_once  'model.php';

$model = new model();

$site = $model->Sites();
//print_r($menu);
//print_r($site);
//print_r($getmenu);
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-dark bg-danger mt-2">
                    <a href="<?=$site[0]['SITE_LINK'];?>" class="text-white mr-md-3"><h4><?=$site[0]['NAME'];?></h4></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <?php
                            if(isset($_GET['id']))
                            {
                                $menu = $model->GetMenu(1, $_GET['id']);
                            foreach($menu as $row) {
                                $item = $model -> GetChild($row['ELEMENT_ID']);
                                    if(!empty($item)) {  ?>
                                        <li class="nav-item dropdown">
                                           <a class="nav-link dropdown-toggle text-white border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?=$row['LABEL'];?>
                                            </a>
                                            <div class="dropdown-menu bg-danger" aria-labelledby="dropdownMenuButton">
                                                <?php foreach($item as $value) { ?>
                                                    <a class="dropdown-item text-white" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a>
                                                <?php } ?>
                                            </div>
                                        </li>
                                    <?php  } else { ?>
                                        <li class="nav-item"><a class="nav-link text-white" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a></li>
                            <?php }  } }
                            else{
                                $menu = $model->GetMenu(1, 1);
                                foreach($menu as $row) {
                                    $item = $model -> GetChild($row['ELEMENT_ID']);
                                    if(!empty($item)) {  ?>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle text-white border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?=$row['LABEL'];?>
                                            </a>
                                            <div class="dropdown-menu bg-danger" aria-labelledby="dropdownMenuButton">
                                                <?php foreach($item as $value) { ?>
                                                    <a class="dropdown-item text-white" href="<?=$value['LINK'];?>"><?=$value['LABEL'];?></a>
                                                <?php } ?>
                                            </div>
                                        </li>
                                    <?php  } else { ?>
                                        <li class="nav-item"><a class="nav-link text-white" href="<?=$row['LINK'];?>"><?=$row['LABEL'];?></a></li>
                                    <?php }  }
                            }?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row mt-2 mb-2">
            <div class="col-12">
                <p><h1>World</h1></p>
            </div>
            <div class="col-12 col-md-8">
                <img src="images/hilary.jpg" class="img-fluid">
            </div>
            <div class="col-12 col-md-4">
                <div class="row h-100 d-flex align-items-between">
                    <div class="col-12 py-2 py-md-0">
                        <img src="images/download%20(1).jpg" class="img-fluid w-100">
                    </div>
                    <div class="col-12 d-flex align-self-end">
                        <img src="images/ronaldo.jpg" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 carousel-image" src="images/PJ_2016.07.07_Modern-News-Consumer_0-01.png" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel-image" src="images/OD_TileImages_News_AtSix.png.2016-10-03T09_48_32+13_00.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel-image" src="images/_90041298_p01tlf61.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 carousel-image" src="images/sydney-9-news-meet-the-team.jpg" alt="Fourth slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="jumbotron jumbotron-fluid mt-2">
            <div class="container">
                <h1 class="display-4 font-advertisement">Place for your advertisement</h1>
            </div>
        </div>

        <!-- Footer -->
        <footer class="page-footer font-small indigo mb-3 ">

            <!-- Footer Links -->
            <div class="container text-center text-md-left">

                <!-- Grid row -->
                <div class="row">

                    <?php $getmenu = $model->GetMenu(1, 27); foreach ($getmenu as $row): ?>
                    <!-- Grid column -->
                    <div class="col-md-2 mx-auto">

                        <!-- Links -->
                        <h5 class="font-weight-bold text-uppercase"><?=$row['LABEL']?></h5>
                        <?php $item = $model -> GetChild($row['ELEMENT_ID']);
                        foreach ($item as $value): ?>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <a href="<?=$value['LINK']?>" class="text-secondary"><?=$value['LABEL']?></a>
                            </li>
                        </ul>
                        <?php endforeach; ?>

                    </div>

                    <?php endforeach; ?>
                    <!-- Grid column -->


        </footer>
        <!-- Footer -->

    </div>
</body>


