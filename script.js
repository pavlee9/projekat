
$(document).ready(function(){

    $('.js-example-basic-multiple').select2({
        width: '100%',
        tags: true
    });



    $('#add_child').click(function() {

        var array = [];
        var elems = $( "[name^='states']").val();
        var site = $('#site_select').val();
        var menu = $('#menu_name').val();
        console.log(elems);
        if(elems == ''){
            $('#validate').show();
            $('#validate').html('Insert empty fields!');
        }
        else {
            $('#add_child').hide();
            $('#validate').hide();
        }
        for(var i=0;i<elems.length;i++)
        {
            $('#tag-list')
                .append('<label>' + elems[i] + ':</label>'+
                    '<select class="js-example-basic-multiple" name="roots'+i+'[]" multiple="multiple" >' +
                    '</select>')
                .append(`<br>`);
        }

        $('.js-example-basic-multiple').select2({
            width: '100%',
            tags: true
        });


        for(var i=0;i<elems.length;i++)
        {
            var arra1 = $("select[name='roots"+i+"[]']").val();
            array.push(arra1);
        }
        //console.log(array);

        $('#save').click(function() {
            $('#exampleModal1').modal('hide');
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {menu: menu, site: site, elems:elems, array:array},
                success: function (data) {
                    $('#exampleModal').modal('hide');
                    $('#s').html('Menu added');
                    //location.reload();
                    setTimeout("$('#s').hide();", 8000);
                }
            })
        });
    });

    $('#select_menu').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        console.log(optionSelected);
        console.log(valueSelected);
        $.ajax({
            url: 'action.php?name='+valueSelected,
            method: 'GET',
            success: function (data) {
                $('#select_div').show();
                $('#roots').html(data);
            }
        })
    });

    $('#submenu').click(function () {
        var choose_menu = $('#select_menu').val();
        var root = $('#roots').val();
        var childs = $( "[name^='childs']").val();
        if(choose_menu != '' && childs != '')
        {
            $.ajax({
                url: 'action.php',
                method: 'POST',
                data: {choose_menu: choose_menu, root: root, childs : childs},
                success: function (data) {
                    $('#exampleModal1').modal('hide');
                    $('#s').html('Submenu added');
                    setTimeout(location.reload.bind(location), 1000);
                }
            })
        }
        else
        {
            $('#validate1').show();
            $('#validate1').html('Insert empty fields!');
        }
    });

    $('#save').click(function(){
        var site = $('#site_select').val();
        var elems = $( "[name^='states']").val();
        var menu = $('#menu_name').val();
        if(menu != '' && elems != '') {
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {menu: menu, site: site, elems: elems},
                success: function (data) {
                    $('#exampleModal').modal('hide');
                    $('#s').html('Menu added');
                    setTimeout(location.reload.bind(location), 1000);
                }
            })
        }
        else
        {
            $('#validate').show();
            $('#validate').html('Insert empty fields!');
        }

    });


    $('#sort').change(function(){
        var sort = $('#sort').val();
        if (sort=='root items by sites')
            location.reload();
        $.ajax({
            url: "action.php",
            method: "POST",
            data: {sort: sort},
            success: function (data) {
                $('#table').html(data);
            }
        })
    });


    $("#btn-add-root").click(function() {
        $("#root").val('')
        $("#show").css("display","block");
    });


});




